#!/bin/bash

read -p "La posicion del corredor es :" posicion

case $posicion in

        1)
              echo "Medalla de Oro";;
        2)
              echo "Medalla de Plata";;
        3)
              echo "Medalla de Bronce";;
        [456789])
              echo "Tienes un Diploma";;
        10|11|12|13|14|15|16|17|18|19)
              echo "Has quedado muy bien";;
        *)
              echo "Gracias por participar";;

esac
